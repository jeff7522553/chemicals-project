var express = require('express');
var path = require("path");
var bodyParser = require('body-parser');
var mongo = require("mongoose");

var db = mongo.connect("mongodb://localhost:27017/chemicalsdb", function (err, response) {
    if (err) { console.log(err); }
    else { console.log('Connected to ' + db, ' + '); }
});


var app = express()
app.use(bodyParser());
app.use(bodyParser.json({ limit: '5mb' }));
app.use(bodyParser.urlencoded({ extended: true }));

app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4200');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

var Schema = mongo.Schema;

var chemicalsSchema = new Schema({
    any: {}
});




// app.post("/api/SaveFund", function (req, res) {
//     var mod = new model(req.body);
//     if (req.body.mode == "Save") {
//         mod.save(function (err, data) {
//             if (err) {
//                 res.send(err);
//             }
//             else {
//                 res.send({ data: "Record has been Inserted..!!" });
//             }
//         });
//     }
//     else {
//         model.findByIdAndUpdate(req.body.id, { name: req.body.name, address: req.body.address },
//             function (err, data) {
//                 if (err) {
//                     res.send(err);
//                 }
//                 else {
//                     res.send({ data: "Record has been Updated..!!" });
//                 }
//             });


//     }
// })

// app.post("/api/deleteFund", function (req, res) {
//     model.remove({ _id: req.body.id }, function (err) {
//         if (err) {
//             res.send(err);
//         }
//         else {
//             res.send({ data: "Record has been Deleted..!!" });
//         }
//     });
// })



app.get("/api/getMarkedContent", function (req, res) {
    console.log("getMarkedContent")
    let model = mongo.model('markedContent', chemicalsSchema, 'markedContent');
    model.find({}, function (err, data) {
        if (err) {
            res.send(err);
        }
        else {
            console.log("Susscess!")
            res.send(data);
        }
    });
})

app.get("/api/getKeyWordsCounts", function (req, res) {
    console.log("getKeyWordsCounts")
    let model = mongo.model('keyWordCounts', chemicalsSchema, 'keyWordCounts');
    model.find({}, function (err, data) {
        if (err) {
            res.send(err);
        }
        else {
            res.send(data);
        }
    });
})

app.get("/api/getKeyWordsTf", function (req, res) {
    console.log("getKeyWordsTf")
    let model = mongo.model('keyWordTf', chemicalsSchema, 'keyWordTf');
    model.find({}, function (err, data) {
        if (err) {
            res.send(err);
        }
        else {
            res.send(data);
        }
    });
})


app.get("/api/getR1Counts", function (req, res) {
  console.log("getR1Counts")
  let model = mongo.model('R1Counts', chemicalsSchema, 'R1Counts');
  model.find({}, function (err, data) {
      if (err) {
          res.send(err);
      }
      else {
          res.send(data);
      }
  });
})

app.get("/api/getR2Counts", function (req, res) {
  console.log("getR2Counts")
  let model = mongo.model('R2Counts', chemicalsSchema, 'R2Counts');
  model.find({}, function (err, data) {
      if (err) {
          res.send(err);
      }
      else {
          res.send(data);
      }
  });
})

app.get("/api/getR3Counts", function (req, res) {
  console.log("getR3Counts")
  let model = mongo.model('R3Counts', chemicalsSchema, 'R3Counts');
  model.find({}, function (err, data) {
      if (err) {
          res.send(err);
      }
      else {
          res.send(data);
      }
  });
})

//  R21 ~ R24
app.get("/api/getR21Counts", function (req, res) {
  console.log("getR21Counts")
  let model = mongo.model('R21Counts', chemicalsSchema, 'R21Counts');
  model.find({}, function (err, data) {
      if (err) {
          res.send(err);
      }
      else {
          res.send(data);
      }
  });
})
app.get("/api/getR22Counts", function (req, res) {
  console.log("getR22Counts")
  let model = mongo.model('R22Counts', chemicalsSchema, 'R22Counts');
  model.find({}, function (err, data) {
      if (err) {
          res.send(err);
      }
      else {
          res.send(data);
      }
  });
})
app.get("/api/getR23Counts", function (req, res) {
  console.log("getR23Counts")
  let model = mongo.model('R23Counts', chemicalsSchema, 'R23Counts');
  model.find({}, function (err, data) {
      if (err) {
          res.send(err);
      }
      else {
          res.send(data);
      }
  });
})
app.get("/api/getR24Counts", function (req, res) {
  console.log("getR24Counts")
  let model = mongo.model('R24Counts', chemicalsSchema, 'R24Counts');
  model.find({}, function (err, data) {
      if (err) {
          res.send(err);
      }
      else {
          res.send(data);
      }
  });
})


//  R31 ~ R34
app.get("/api/getR31Counts", function (req, res) {
  console.log("getR31Counts")
  let model = mongo.model('R31Counts', chemicalsSchema, 'R31Counts');
  model.find({}, function (err, data) {
      if (err) {
          res.send(err);
      }
      else {
          res.send(data);
      }
  });
})
app.get("/api/getR32Counts", function (req, res) {
  console.log("getR32Counts")
  let model = mongo.model('R32Counts', chemicalsSchema, 'R32Counts');
  model.find({}, function (err, data) {
      if (err) {
          res.send(err);
      }
      else {
          res.send(data);
      }
  });
})
app.get("/api/getR33Counts", function (req, res) {
  console.log("getR33Counts")
  let model = mongo.model('R33Counts', chemicalsSchema, 'R33Counts');
  model.find({}, function (err, data) {
      if (err) {
          res.send(err);
      }
      else {
          res.send(data);
      }
  });
})
app.get("/api/getR34Counts", function (req, res) {
  console.log("getR34Counts")
  let model = mongo.model('R34Counts', chemicalsSchema, 'R34Counts');
  model.find({}, function (err, data) {
      if (err) {
          res.send(err);
      }
      else {
          res.send(data);
      }
  });
})




//
//  Summary
//
app.get("/api/getR1Summary", function (req, res) {
  console.log("getR1Summary")
  let model = mongo.model('R1Summary', chemicalsSchema, 'R1Summary');
  model.find({}, function (err, data) {
      if (err) {
          res.send(err);
      }
      else {
          res.send(data);
      }
  });
})
app.get("/api/getR2Summary", function (req, res) {
  console.log("getR2Summary")
  let model = mongo.model('R2Summary', chemicalsSchema, 'R2Summary');
  model.find({}, function (err, data) {
      if (err) {
          res.send(err);
      }
      else {
          res.send(data);
      }
  });
})
app.get("/api/getR3Summary", function (req, res) {
  console.log("getR3Summary")
  let model = mongo.model('R3Summary', chemicalsSchema, 'R3Summary');
  model.find({}, function (err, data) {
      if (err) {
          res.send(err);
      }
      else {
          res.send(data);
      }
  });
})


app.listen(8080, function () {
    console.log('Example app listening on port 8080!')
})
