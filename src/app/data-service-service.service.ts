import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable, from, of } from 'rxjs';
import { map } from 'rxjs/operators';
import * as io from 'socket.io-client';

@Injectable({
  providedIn: 'root'
})
export class DataServiceService {
  private url = 'http://localhost:8080/api';
  private socket;

  constructor(private http: Http) {}

  getMarkedContent() {
    console.log('getMarkedContent  !!! ');
    return this.http
      .get(`${this.url}/getMarkedContent`)
      .pipe(map((response: Response) => response.json()));
  }

  getKeyWordsCounts() {
    console.log('getKeyWordsCounts  !!! ');
    return this.http
      .get(`${this.url}/getKeyWordsCounts`)
      .pipe(map((response: Response) => response.json()));
  }
  getKeyWordsTf() {
    console.log('getKeyWordsTf  !!! ');
    return this.http
      .get(`${this.url}/getKeyWordsTf`)
      .pipe(map((response: Response) => response.json()));
  }

  getR1Counts() {
    console.log('getR1Counts  !!! ');
    return this.http
      .get(`${this.url}/getR1Counts`)
      .pipe(map((response: Response) => response.json()));
  }
  getR2Counts() {
    console.log('getR2Counts  !!! ');
    return this.http
      .get(`${this.url}/getR2Counts`)
      .pipe(map((response: Response) => response.json()));
  }
  getR3Counts() {
    console.log('getR3Counts  !!! ');
    return this.http
      .get(`${this.url}/getR3Counts`)
      .pipe(map((response: Response) => response.json()));
  }

  // R21 ~~ R24
  getR21Counts() {
    console.log('getR21Counts  !!! ');
    return this.http
      .get(`${this.url}/getR21Counts`)
      .pipe(map((response: Response) => response.json()));
  }
  getR22Counts() {
    console.log('getR22Counts  !!! ');
    return this.http
      .get(`${this.url}/getR22Counts`)
      .pipe(map((response: Response) => response.json()));
  }
  getR23Counts() {
    console.log('getR23Counts  !!! ');
    return this.http
      .get(`${this.url}/getR23Counts`)
      .pipe(map((response: Response) => response.json()));
  }
  getR24Counts() {
    console.log('getR24Counts  !!! ');
    return this.http
      .get(`${this.url}/getR24Counts`)
      .pipe(map((response: Response) => response.json()));
  }

  // R31 ~~ R34
  getR31Counts() {
    console.log('getR31Counts  !!! ');
    return this.http
      .get(`${this.url}/getR31Counts`)
      .pipe(map((response: Response) => response.json()));
  }
  getR32Counts() {
    console.log('getR32Counts  !!! ');
    return this.http
      .get(`${this.url}/getR32Counts`)
      .pipe(map((response: Response) => response.json()));
  }
  getR33Counts() {
    console.log('getR33Counts  !!! ');
    return this.http
      .get(`${this.url}/getR33Counts`)
      .pipe(map((response: Response) => response.json()));
  }
  getR34Counts() {
    console.log('getR34Counts  !!! ');
    return this.http
      .get(`${this.url}/getR34Counts`)
      .pipe(map((response: Response) => response.json()));
  }

  //
  //  Summary
  //
  getR1Summary() {
    console.log('getR1Summary  !!! ');
    return this.http
      .get(`${this.url}/getR1Summary`)
      .pipe(map((response: Response) => response.json()));
  }
  getR2Summary() {
    console.log('getR2Summary  !!! ');
    return this.http
      .get(`${this.url}/getR2Summary`)
      .pipe(map((response: Response) => response.json()));
  }
  getR3Summary() {
    console.log('getR3Summary  !!! ');
    return this.http
      .get(`${this.url}/getR3Summary`)
      .pipe(map((response: Response) => response.json()));
  }

  // saveUser(user) {
  //   return this.http.post('http://localhost:8080/api/SaveUser/', user)
  //     .pipe(map((response: Response) => response.json()))

  // }

  // GetUser() {
  //   return this.http.get('http://localhost:8080/api/getUser/')
  //     .pipe(map((response: Response) => response.json()))

  // }
  // deleteUser(id) {
  //   return this.http.post('http://localhost:8080/api/deleteUser/', { 'id': id })
  //     .pipe(map((response: Response) => response.json()))
  // }
}
